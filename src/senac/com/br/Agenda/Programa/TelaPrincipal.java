/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package senac.com.br.Agenda.Programa;


import java.io.IOException;
import javax.swing.JOptionPane;
import jdk.nashorn.internal.codegen.CompilerConstants;
import senac.com.br.Agenda.Contatos.Agenda;
import senac.com.br.Agenda.Contatos.Pessoa;

/**
 *
 * @author Timotei
 */
public class TelaPrincipal extends Padrao {

    private Agenda agenda;
    
    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipal() {
        initComponents();
        this.agenda = new Agenda();
        try{
            agenda.load();
        } catch (IOException ex) {
            showMessageErro("Falha ao carregar arquivo agenda.txt");
        }
        this.atualizar();
    }

    TelaPrincipal(Agenda agenda, TelaPrincipal telaPrincipal, Pessoa pessoa) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jLabelVersao = new javax.swing.JLabel();
        jLabelAgendaDeContatos = new javax.swing.JLabel();
        jLabelAgenda = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jLabelQuantidadeContatos = new javax.swing.JLabel();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuContatos = new javax.swing.JMenu();
        jMenuItemNovosContatos = new javax.swing.JMenuItem();
        jMenuItemPesquisaContatos = new javax.swing.JMenuItem();
        jMenuItemSair = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuAjuda = new javax.swing.JMenu();
        jMenuItemQuemSomos = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ELProperty.create("Tela Principal"), this, org.jdesktop.beansbinding.BeanProperty.create("title"));
        bindingGroup.addBinding(binding);

        jLabelVersao.setText("Versão 1.0");

        jLabelAgendaDeContatos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabelAgendaDeContatos.setText("Agenda De Contatos");

        jLabelAgenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/Agenda.png"))); // NOI18N

        jToolBar1.setRollover(true);

        jLabelQuantidadeContatos.setText("jLabel1");
        jToolBar1.add(jLabelQuantidadeContatos);

        jMenuContatos.setText("Contatos");

        jMenuItemNovosContatos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemNovosContatos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/Adicionar.png"))); // NOI18N
        jMenuItemNovosContatos.setText("Novo Contatos");
        jMenuItemNovosContatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNovosContatosActionPerformed(evt);
            }
        });
        jMenuContatos.add(jMenuItemNovosContatos);

        jMenuItemPesquisaContatos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemPesquisaContatos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/Pesquisar.png"))); // NOI18N
        jMenuItemPesquisaContatos.setText("Pesquisar Contatos");
        jMenuItemPesquisaContatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPesquisaContatosActionPerformed(evt);
            }
        });
        jMenuContatos.add(jMenuItemPesquisaContatos);

        jMenuItemSair.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_MASK));
        jMenuItemSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/Excluir.png"))); // NOI18N
        jMenuItemSair.setText("Sair");
        jMenuItemSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSairActionPerformed(evt);
            }
        });
        jMenuContatos.add(jMenuItemSair);
        jMenuContatos.add(jSeparator1);

        jMenuBar.add(jMenuContatos);

        jMenuAjuda.setText("Ajuda");

        jMenuItemQuemSomos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemQuemSomos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/LoginEnter.png"))); // NOI18N
        jMenuItemQuemSomos.setText("Sobre");
        jMenuItemQuemSomos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuemSomosActionPerformed(evt);
            }
        });
        jMenuAjuda.add(jMenuItemQuemSomos);
        jMenuAjuda.add(jSeparator2);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/senac/com/br/Agenda/Imagens/Pesquisar.png"))); // NOI18N
        jMenuItem1.setText("Ajuda");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenuAjuda.add(jMenuItem1);

        jMenuBar.add(jMenuAjuda);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelAgendaDeContatos)
                        .addGap(26, 26, 26)
                        .addComponent(jLabelVersao))
                    .addComponent(jLabelAgenda))
                .addContainerGap(30, Short.MAX_VALUE))
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelAgendaDeContatos)
                    .addComponent(jLabelVersao))
                .addGap(18, 18, 18)
                .addComponent(jLabelAgenda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bindingGroup.bind();

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemPesquisaContatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPesquisaContatosActionPerformed
        new TelaDePesquisa(agenda, this);
    }//GEN-LAST:event_jMenuItemPesquisaContatosActionPerformed

    private void jMenuItemNovosContatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNovosContatosActionPerformed
    new TelaDeCadastro(agenda, this);
    }//GEN-LAST:event_jMenuItemNovosContatosActionPerformed

    private void jMenuItemSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSairActionPerformed
      this.salvarAgenda();
    }//GEN-LAST:event_jMenuItemSairActionPerformed

    private void jMenuItemQuemSomosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuemSomosActionPerformed
        JOptionPane.showMessageDialog(jMenuItemQuemSomos, "Autor: Samuel Batista Ribeiro\n"
                + "Turma-721(2019) - Tecnico em Informatica\n"
                + "Versão: 1.0", "Sobre", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItemQuemSomosActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Ajuda ajuda = new Ajuda();   
    }//GEN-LAST:event_jMenuItem1ActionPerformed
 private void salvarAgenda(java.awt.event.WindowEvent evt) {
      this.salvarAgenda();
 } 
    
    public void atualizar() {
        this.jLabelQuantidadeContatos.setText("Contatos:" + this.agenda.getQuantidadeContatos());
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelAgenda;
    private javax.swing.JLabel jLabelAgendaDeContatos;
    private javax.swing.JLabel jLabelQuantidadeContatos;
    private javax.swing.JLabel jLabelVersao;
    private javax.swing.JMenu jMenuAjuda;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuContatos;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItemNovosContatos;
    private javax.swing.JMenuItem jMenuItemPesquisaContatos;
    private javax.swing.JMenuItem jMenuItemQuemSomos;
    private javax.swing.JMenuItem jMenuItemSair;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JToolBar jToolBar1;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    
    private void salvarAgenda() {
        try {
            this.agenda.exportar();

            System.exit(0);

        } catch (IOException ex) {
            showMessageErro("Erro ao gravar arquivo.");
        }

    }






}
